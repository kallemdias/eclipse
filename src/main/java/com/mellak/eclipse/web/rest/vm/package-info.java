/**
 * View Models used by Spring MVC REST controllers.
 */
package com.mellak.eclipse.web.rest.vm;
